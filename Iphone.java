public class Iphone{
	public int numberOfContacts;
	public int numberOfSongs;
	public String yourUsername;
	
	public void appleMusic(){
		if(this.numberOfSongs > 30){
			System.out.println("You listen to music a lot since you have more than 30 songs!");
		}else if(this.numberOfSongs <= 30 && this.numberOfSongs >= 10){
			System.out.println("You do listen to music sometimes since you have 10 to 30 songs!");
		}else{
			System.out.println("You listen to music very rarely since you have less than 10 songs!");	
		}
	}
}