import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
	Scanner sc = new Scanner(System.in);
	//array with 4 Iphone	
	Iphone[] product = new Iphone[4];
	
	for(int i = 0;i< product.length; i++){
		product[i] = new Iphone();
		//New Iphone message 
		System.out.println("");
		System.out.println("---New Iphone---");
		System.out.println("Enter the number of contacts in your Iphone: ");
		product[i].numberOfContacts = sc.nextInt();
		System.out.println("Enter the number of songs in your Iphone: ");
		product[i].numberOfSongs = sc.nextInt();
		System.out.println("Enter your username for the Iphone(one word): ");
		product[i].yourUsername = sc.next();
		System.out.println("");		
	} 
	//Last Iphone field information
	System.out.println("The number of contacts in the last Iphone: " +product[product.length - 1].numberOfContacts);
	System.out.println("The number of songs in the last Iphone: " +product[product.length - 1].numberOfSongs);
	System.out.println("The username in the last Iphone: " +product[product.length - 1].yourUsername);
	System.out.println("");
	//Calling the instance method appleMusic on the last Iphone(product)
	product[product.length - 1].appleMusic();
	}
}